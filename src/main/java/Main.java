import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vahicle.*;

import java.util.*;

public class Main {
    private static final Map<Class<? extends Vehicle>, List<Vehicle>> vehicles = new HashMap<>();
    private static final Scanner scanner = new Scanner(System.in);
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        initVehicles();

        while (true) {
            logger.info("Podaj kategorię lub opuść program (EXIT)");
            String input = scanner.nextLine().toUpperCase();
            Vehicle fastestVehicle = null;
            switch (input) {
                case "CAR":
                    fastestVehicle= getFastestVehicle(Car.class);
                    break;
                case "SHIP":
                    fastestVehicle = getFastestVehicle(Ship.class);
                    break;
                case "PLANE":
                    fastestVehicle = getFastestVehicle(Plane.class);
                    break;
                case "BICYCLE":
                    fastestVehicle = getFastestVehicle(Bicycle.class);
                    break;
                case "ALL":
                    fastestVehicle = getFastestVehicle();
                    break;
                case "EXIT":
                    return;
                default:
                    logger.warn("Podaj prawidłową opcję");
            }
            if (fastestVehicle != null) logger.info(
                    "Pojazd {} producenta {}, jest najszybszy (maksymalna prędkość to={})\n",
                    fastestVehicle.getClass().getSimpleName().toUpperCase(),
                    fastestVehicle.getManufacturer(),
                    fastestVehicle.getMaxSpeed()
            );
        }
    }

    private static void initVehicles() {
        vehicles.put(Car.class, Arrays.asList(
                new Car(220, "Ferrari"),
                new Car(120, "Opel")
        ));
        vehicles.put(Ship.class, Arrays.asList(
                new Ship(70, "Fake mano"),
                new Ship(100, "Super ship")
        ));
        vehicles.put(Plane.class, Arrays.asList(
                new Plane(520, "Plane manufacturer A"),
                new Plane(550, "Plane manufacturer B")
        ));
        vehicles.put(Bicycle.class, Arrays.asList(
                new Bicycle(15, "Wigry"),
                new Bicycle(70, "Cross")
        ));
    }

    private static Vehicle getFastestVehicle(Class<? extends Vehicle> vehicleClass) {
        return vehicles.get(vehicleClass).stream().max(Comparator.naturalOrder()).orElse(null);
    }

    private static Vehicle getFastestVehicle() {
        Vehicle fastestVehicle = null;

        for (var vehicleList : vehicles.values()) {
            for (Vehicle vehicle : vehicleList) {
                if (fastestVehicle == null || vehicle.compareTo(fastestVehicle) > 0) fastestVehicle = vehicle;
            }
        }

        return fastestVehicle;
    }
}

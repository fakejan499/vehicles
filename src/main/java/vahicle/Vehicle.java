package vahicle;

public class Vehicle implements Comparable<Vehicle>{
    private final int maxSpeed;
    private final String manufacturer;

    protected Vehicle(int maxSpeed, String manufacturer) {
        this.maxSpeed = maxSpeed;
        this.manufacturer = manufacturer;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    @Override
    public int compareTo(Vehicle o) {
        return maxSpeed - o.maxSpeed;
    }
}
